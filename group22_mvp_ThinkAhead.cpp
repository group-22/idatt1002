#include <iostream>
#include <iomanip>      // std::setw, std::left
#include <stdio.h>
#include <vector>
#include <string>
#include <map>  
#include <tuple>
#include <fstream>      // Write and read from file
#include <cctype>       // toupper
#include <algorithm>    // std::find_if

const int   MAXCHAR = 100;

// UI
int     screen_width        = 41 + 100, // should never be less than 41
        id_width            = 3 + (screen_width-41)/24,
        task_width          = 5 + (screen_width-41)/4,
        date_width          = 5 + (screen_width-41)/24,
        description_width   = 12 + (screen_width-41)/2,
        status_width        = 7 + (screen_width-41)/8,
        priority_width      = 9 + (screen_width-41)/24;

std::string filename            = "data.dta";

/**
 * "Clears" the screen (by outputting a bunch of blank lines)
 */
void ClearScreen() {
    std::cout << std::string( 100, '\n' );
}

/**
 *  Leser og returnerer et heltall mellom to gitte grenser.
 *
 *  @param   t    - Ledetekst til brukeren n�r ber om input/et tall
 *  @param   min  - Minimum for innlest og godtatt tallverdi
 *  @param   max  - Maksimum for innlest og godtatt tallverdi
 *
 *  @return  Godtatt verdi i intervallet 'min' - 'max'
 */
int lesInt(const char* t, const int min, const int max)  {
    char buffer[MAXCHAR] = "";
    int  tall = 0;
    bool feil = false;

    do {
        feil = false;
        std::cout << t << " (" << min << " - " << max << "):  ";
        std::cin.getline(buffer, MAXCHAR);
        tall = atoi(buffer);
        if (tall == 0 && buffer[0] != '0')
        {  feil = true;  }
    } while (feil  ||  tall < min  ||  tall > max);

    return tall;
}

/**
 *  Leser og returnerer ett (upcaset) tegn.
 *
 *  @param   t  - Ledetekst til brukeren når ber om ett tegn
 *
 *  @return  Ett (upcaset) tegn.
 */
 char lesChar(const char* t)  {
     char tegn;
     std::cout << t << ":  ";
     std::cin >> tegn;  std::cin.ignore(MAXCHAR, '\n');
     return (toupper(tegn));
}

/**
 * Explodes a string, splitting it on each occurance of a given char.
 * @param s - The string.
 * @param c - The character.
 * @return The split string as a vector of substrings.
 */
const std::vector <std::string> explode(const std::string& s, const char& c) {
	std::string buff{""};
	std::vector <std::string> v;
	
	for(auto n:s)
	{
		if(n != c) buff+=n; else
		if(n == c && buff != "") { v.push_back(buff); buff = ""; }
	}
	if(buff != "") v.push_back(buff);
	
	return v;
}

/**
 * Replaces every occurance of a given character with another.
 * @param s - The string.
 * @param ci - The character that is replaced.
 * @param co - The character which ci is replaced with.
 * @return The string, but every occurance of ci is replaced with co.
 */
std::string replaceChar(std::string s, char ci, char co) {
    for (int i=0; i<s.length(); i++) {
        if (s[i] == ci) {
            if (co == '\0')
                s.erase(s.begin() + i);
            else
                s[i] = co;
        }
    }
    return s;
}

/**
 * Checks whether or not a string is a number.
 * @param s - The string.
 * @return Whether or not the string is a number.
 */
bool is_number(const std::string& s) {
    std::string::const_iterator it = s.begin();
    while (it != s.end() && std::isdigit(*it)) ++it;
    return !s.empty() && it == s.end();
}

/**
 * A simple class for unordered maps with char and strings in them.
 * (Unordered meaning that the order in which you insert object is the order that is retrieved)
 */
class Unordered_map {
private:
    std::map <char, std::string> map;
    std::vector <char> order;

public:
    /**
     * Default constructor.
     */
    Unordered_map () {}

    /**
     * Initializer list constructor with chars and strings.
     */
    Unordered_map ( std::initializer_list <std::pair <char, std::string>> il ) {
        for (auto it = il.begin(); it != il.end(); it++) {
            map.insert(*it); order.push_back(it->first);
        }
    }

    /**
     * Pair list constructor.
     */
    Unordered_map ( std::vector <std::pair <char, std::string>> il ) {
         for (auto it = il.begin(); it != il.end(); it++) {
            map.insert(*it); order.push_back(it->first);
        }       
    }

    /**
     * Gets the pair at a given index.
     * @param index - The index.
     * @return The pair at the given index.
     */
    std::pair <char, std::string> operator[] (int index) {
        return std::pair <char, std::string> (order[index], map[order[index]]); 
    }

    /**
     * Gets the size of the map.
     * @return The size of the map.
     */
    int size() {
        return order.size();
    }
};

/**
 * Creates a menu that is closed when the user has entered a valid option.
 * 
 * @param options - A map of each option (char) aswell as their corresponding description (string).
 * @param columns - How many columns to display the options in.
 * @param spacing - The spacing between each column.
 * 
 * @return The option which the user selected.
 */
char createMenu (Unordered_map options, int columns = 2, int spacing = 25) {
    // Write out the options and their description
    for (int i=0; i<options.size(); i++) {
        std::pair <char, std::string> it = options[i];
        
        char        c = it.first;                       // The character for the option
        std::string s = it.second,                      // The string describing the option
                    t = std::string(1,c) + " - " + s;   // Character and string merged. ex: "A - Add item"

        std::cout << std::setw(spacing) << std::left << t;
        if (i%columns==columns-1) std::cout << std::endl;
    }
    std::cout << "\n";

    // Require user input
    bool done = false;
    char input = '\0';
    while (!done) {
        input = lesChar("Input");
        for (int i=0; i<options.size(); i++) {
            std::pair <char, std::string> it = options[i];

            char    c = it.first;
            if (input == toupper(c)) {
                done = true;
                break;
            }
        }
    }

    return input;
}

/**
 * Converts a string-date to two integers, day and month.
 * @param ddmm - The day and month as a string with the format "{dd}/{mm}".
 * @return A pair with the day and month as integers.
 */
std::pair <int,int> convDate (std::string ddmm) {
    std::vector <std::string> args = explode(ddmm, '/');

    // Check if there's enough args
    if (args.size() < 2)
        return std::pair <int,int> (NULL, NULL);
    std::pair <int,int> ret (std::stoi(args[0]), std::stoi(args[1]));

    // Check if the day and month is within boundaries
    if (ret.first < 1 || ret.first > 31 || ret.second < 1 || ret.second > 12)
        return std::pair <int,int> (NULL, NULL);

    return ret;
}

/**
 * Converts a series of numbers in string-format to separate numbers. Note: Negative numbers doesn't work.
 * @param nums - The numbers with the following format {int}, {int}, {int}-{int} as a string.
 * @return A vector with all the numbers in the ranges.
 */
std::vector <int> convNums (std::string nums, bool onlyUniqueNumbers = true) {
    nums = replaceChar(nums, ' ', '\0'); // Remove spaces
    std::vector <int> ret;
    std::vector <std::string> commas = explode(nums, ','); // Split on ','
    for (int i=0; i<commas.size(); i++) {
        std::string comma = commas[i];
        std::vector <std::string> ranges = explode(comma, '-'); // Split on '-'
        if (ranges.size() >= 2 && is_number(ranges[0]) && is_number(ranges[1])) {
            int start = std::stoi(ranges[0]),
                end   = std::stoi(ranges[1]);
            for (int j=start; j<=end; j++) {
                if (!onlyUniqueNumbers || std::find(ret.begin(), ret.end(), j) == ret.end()) ret.push_back(j);
            }
        } else if (is_number(comma)) 
            if (!onlyUniqueNumbers || std::find(ret.begin(), ret.end(), std::stoi(comma)) == ret.end()) ret.push_back(std::stoi(comma));
    } 
    return ret;
}


/**
 * A simple class for Tasks.
 */
class Task {
private:
    std::string     name,           //!<  The Task's name.
                    description,    //!<  The Task's description.
                    status,         //!<  The Task's status.
                    list;           //!<  Which list the Task lies within.
    int             dueDay,         //!<  The Task's due day (1-31).
                    dueMonth,       //!<  The Task's due month (1-12).
                    priority;       //!<  The Task's priority (0-10).

public:
    /**
     * Getter functions
     */
    std::string getName ()          { return name; }
    std::string getDescription ()   { return description; }
    std::string getStatus ()        { return status; }
    std::string getList ()          { return list; }
    int         getStatusNu ()      { return (status == "Finished" ? 0 : (status == "In progress") ? 1 : (status == "On hold" ? 2 : (status == "Not started" ? 3 : 4))); }
    int         getDueDay ()        { return dueDay; }
    int         getDueMonth ()      { return dueMonth; }
    int         getPriority ()      { return priority; }

    /**
     * Setter functions
     */
    void setName        ( std::string name )        { this->name = name; }
    void setDescription ( std::string description ) { this->description = description; }
    void setStatus      ( std::string status )      { this->status = status; }
    void setList        ( std::string list )        { this->list = list; }
    void setDueDay      ( int dueDay )              { this->dueDay = dueDay; }
    void setDueMonth    ( int dueMonth )            { this->dueMonth = dueMonth; }
    void setPriority    ( int priority )            { this->priority = priority; }

    /**
     * Outputs the Task's data in a raw format.
     */
    void skrivData_raw (std::ofstream& stream) {
            stream      << name         << "\t"
                        << description  << "\t"
                        << dueDay       << "\t"
                        << dueMonth     << "\t"
                        << priority     << "\t"
                        << status       << "\t"
                        << list         << "\n";
    }

    /**
     * Outputs the Task's data in a row format.
     */
    void skrivData_row () {
        // Shorten names if necessary
        std::string s_name = (name.length() > task_width) ? ( name.substr(0,task_width-3) + "..." ) : name,
                    s_date = std::to_string(dueDay) + std::string(1,'/') + std::to_string(dueMonth),
                    s_description = (description.length() > description_width) ? ( description.substr(0,description_width-3) + "..." ) : description,
                    s_status = (status.length() > status_width) ? ( status.substr(0,status_width-3) + "..." ) : status;

        std::cout   << std::setw(task_width)        << std::left    << s_name
                    << std::setw(date_width)        << std::left    << s_date
                    << std::setw(description_width) << std::left    << s_description
                    << std::setw(status_width)      << std::left    << s_status
                    << std::setw(priority_width)    << std::left    << priority;
        std::cout   << std::endl;
    }    


    /**
     * Requests inputs for all data members. 
     * Will loop until the user verifies changes.
     */
    void lesData () {
        // Temporary storage for new name, description, etc.
        std::string     nname,
                        ndescription,
                        nlist,
                        tdate;
        int             ndueDay,
                        ndueMonth,
                        npriority;

        bool userIsSure = false;
        while (!userIsSure) {
            // Name
            std::cout << "Task name: "; std::getline(std::cin, nname);

            // Description
            std::cout << "Description: "; std::getline(std::cin, ndescription);

            // Date
            std::pair <int,int> tdate_int (NULL, NULL);
            while (tdate_int.first == NULL || tdate_int.second == NULL) {
                std::cout << "Due date (dd/mm): "; std::getline(std::cin, tdate); tdate_int = convDate(tdate); 
            }
            ndueDay = tdate_int.first; 
            ndueMonth = tdate_int.second;

            // Priority
            npriority = lesInt("Priority", 0, 10);

            // List
            std::cout << "List (name): "; std::getline(std::cin, nlist);

            // Ask user to verify changes.
            std::cout << "\nAre you sure you entered the right information?\n";
            char userVerify = createMenu(
                Unordered_map {
                    {'Y', "Yes, continue"},
                    {'N', "No, re-enter information"},
                    {'Q', "Quit, back to overview"}
                },
                1
            );

            switch (userVerify) {
                case 'Y':
                    userIsSure = true;
                    break;
                
                case 'N':
                    std::cout << "\nWhat do you want to re-enter?" << std::endl;
                    char userChangeInfo; userChangeInfo = createMenu(
                        Unordered_map {
                            {'E', "Everything"},    {'T', "Task name"},     
                            {'C', "Description"},   {'D', "Due date"},      
                            {'P', "Priority"},      {'L', "List"},    
                            {'N', "Nothing"}
                        },
                        2
                    );

                    switch (userChangeInfo) {
                        case 'E':
                            userIsSure = false;
                            break;

                        case 'N':
                            userIsSure = true;
                            break;

                        case 'T':
                            std::cout << "Re-enter task name: "; std::getline(std::cin, nname);
                            userIsSure = true;  
                            break;

                        case 'C':
                            std::cout << "Re-enter description: "; std::getline(std::cin, ndescription);
                            userIsSure = true;
                            break;

                        case 'D':
                            tdate_int.first = NULL; tdate_int.second = NULL;
                            while (tdate_int.first == NULL || tdate_int.second == NULL) {
                                std::cout << "Re-enter due date (dd/mm): "; std::getline(std::cin, tdate); tdate_int = convDate(tdate); 
                            }
                            ndueDay = tdate_int.first; 
                            ndueMonth = tdate_int.second;
                            userIsSure = true;
                            break;

                        case 'P':
                            npriority = lesInt("Re-enter priority", 0, 10);
                            userIsSure = true;
                            break;

                        case 'L':
                            std::cout << "Re-enter list (name): "; std::getline(std::cin, nlist);
                            userIsSure = true;
                            break;
                    }
                    break;

                case 'Q':
                    goto done;

            }
        }

        // Commit changes
        name = nname;
        description = ndescription;
        dueDay = ndueDay;
        dueMonth = ndueMonth;
        priority = npriority;
        list = nlist;

        done: // Label for instantly aborting without changing anything
        std::cout << "\n";
    }


    /**
     * Writes out options for status and asks for input.
     */
    void lesStatus () {
        std::cout << "\nNew status" << std::endl;
        char userSetStatus; userSetStatus = createMenu (
            Unordered_map {
                {'F', "Finished"},
                {'I', "In progress"},
                {'O', "On hold"},
                {'N', "Not started"},
                {'Q', "Abort changing status"}
            },
            1
        );

        switch (userSetStatus) {
            case 'Q':
                break;

            case 'F':
                status = "Finished";
                break;

            case 'I':
                status = "In progress";
                break;

            case 'O':
                status = "On hold";
                break;

            case 'N':
                status = "Not started";
                break;
        }
    }

    /**
     * Default constructor.
     */
    Task () {}

    /**
     * Lesdata constructor.
     * @param status - The status of the task.
     * @see Task::lesData()
     */
    Task (std::string status) { lesData(); this->status = status; }
};


// Globals
std::vector <Task*>     gTasks_byPriority,  ///<  All Tasks, sorted by priority specifically.
                        gTasks_byDate,      ///<  All Tasks, sorted by date specifically.
                        gTasks_byStatus;    ///<  All Tasks, sorted by status specifically.

bool                    invertedSorting = false;            ///<  Whether or not sorting is inverted.
std::vector <Task*>*    gTasks_ptr = &gTasks_byPriority;    ///<  Pointer pointing to the current task list (the one we want to sort by)


/**
 * Gets all tasks grouped by list (name).
 * @return A map of each list name with their corresponding tasks, sorted.
 */
std::map <std::string, std::vector <Task*>> getTasks () {
    std::map <std::string, std::vector <Task*>> groupedTasks;

    if (invertedSorting) { 
        for (int i=(*gTasks_ptr).size()-1; i>=0; i--) {
            Task* task = (*gTasks_ptr)[i];
            if (groupedTasks.find(task->getList()) != groupedTasks.end())
                groupedTasks[task->getList()].push_back(task);
            else
                groupedTasks.insert( std::pair <std::string, std::vector <Task*>> (task->getList(), std::vector <Task*> {task}) );
        }
        return groupedTasks;
    
    } else {
        for (int i=0; i<(*gTasks_ptr).size(); i++) {
            Task* task = (*gTasks_ptr)[i];
            if (groupedTasks.find(task->getList()) != groupedTasks.end())
                groupedTasks[task->getList()].push_back(task);
            else
                groupedTasks.insert( std::pair <std::string, std::vector <Task*>> (task->getList(), std::vector <Task*> {task}) );
        }
        return groupedTasks;
    }
}


/**
 * Gets the task at the specific index.
 * @param index - The index.
 * @return The task at the given index.
 */
Task* getTask (int index) {
    std::map <std::string, std::vector <Task*>> groupedTasks = getTasks();

    int i0 = 0;
    for (auto it = groupedTasks.begin(); it != groupedTasks.end(); it++) {
        for (int i=0; i<it->second.size(); i++, i0++) {
            if (i0 == index) return it->second[i];
        }
    }

    return nullptr;
}


/**
 * Writes each Task's data.
 */
void skrivTasks () {
    std::string s_date   = "Date"; if (gTasks_ptr == &gTasks_byDate) s_date += (invertedSorting ? " ^" : " v");
    std::string s_status = "Status"; if (gTasks_ptr == &gTasks_byStatus) s_status += (invertedSorting ? " ^" : " v");
    std::string s_priority = "Priority"; if (gTasks_ptr == &gTasks_byPriority) s_priority += (invertedSorting ? " ^" : " v");

    std::cout << "To-Do List" << std::endl;
    std::cout << std::string(screen_width,'-') << "\n";

    // Group tasks by list
    std::map <std::string, std::vector <Task*>> groupedTasks = getTasks();

    // Iterate through groups (lists) and print data of tasks
    int i0 = 0;
    for (auto it = groupedTasks.begin(); it != groupedTasks.end(); it++) {
        std::cout << it->first << std::endl;
        std::cout   << std::setw(id_width)          << std::left    << "#"
                    << std::setw(task_width)        << std::left    << "Task"
                    << std::setw(date_width)        << std::left    << s_date
                    << std::setw(description_width) << std::left    << "Description"
                    << std::setw(status_width)      << std::left    << s_status
                    << std::setw(priority_width)    << std::left    << s_priority
                    << std::endl;

        for (int i=0; i<it->second.size(); i++, i0++) {
            Task* task = it->second[i];
            std::cout << std::string(screen_width,'-') << "\n";
            std::cout << std::setw(id_width) << std::left << i0+1;
            task->skrivData_row();
        }

        std::cout << std::string(screen_width,'-') << "\n\n";
    }
}


/**
 * Gets the available menu options.
 * @return The available options as an unordered map.
 */
Unordered_map getMenuOptions () {
    std::vector <std::pair <char, std::string>> menuOptions;

    menuOptions.push_back( std::pair <char, std::string> ('A', "Add new task") );

    if ((*gTasks_ptr).size() > 0) {
        menuOptions.push_back( std::pair <char, std::string> ('E', "Edit task status") );
        menuOptions.push_back( std::pair <char, std::string> ('D', "Delete task(s)") );
    }

    if ((*gTasks_ptr).size() > 1) 
        menuOptions.push_back( std::pair <char, std::string> ('S', "Sort entries by...") );

    menuOptions.push_back( std::pair <char, std::string> ('W', "Change width of text-window") );
    menuOptions.push_back( std::pair <char, std::string> ('Q', "Exit program") );

    return Unordered_map (menuOptions);
}


/**
 * Binds a task to all associated vectors, and sorts it into the right spots.
 * @param task - The task
 */
void bindTask (Task* task) {
    // If there are other Tasks, put this one into the right spot in the global vectors
    if ((*gTasks_ptr).size() > 0) {
        for (int i=0; i<gTasks_byDate.size(); i++) {
            if (gTasks_byDate[i]->getDueMonth()*31 + gTasks_byDate[i]->getDueDay() < task->getDueMonth()*31 + task->getDueDay()) {
                gTasks_byDate.insert(gTasks_byDate.begin() + i, task);
                break;
            } else if (i == gTasks_byDate.size()-1) {
                gTasks_byDate.push_back(task);
                break;
            }
        }

        for (int i=0; i<gTasks_byPriority.size(); i++) {
            if (gTasks_byPriority[i]->getPriority() < task->getPriority()) {
                gTasks_byPriority.insert(gTasks_byPriority.begin() + i, task);
                break;
            } else if (i == gTasks_byPriority.size()-1) {
                gTasks_byPriority.push_back(task);
                break;
            }
        }

        for (int i=0; i<gTasks_byStatus.size(); i++) {
            if (gTasks_byStatus[i]->getStatusNu() < task->getStatusNu()) {
                gTasks_byStatus.insert(gTasks_byStatus.begin() + i, task);
                break;
            } else if (i == gTasks_byStatus.size()-1) {
                gTasks_byStatus.push_back(task);
                break;
            }
        }
    
    // Or if its the first, just shove it in
    } else {
        gTasks_byDate.push_back(task);
        gTasks_byPriority.push_back(task);
        gTasks_byStatus.push_back(task);
    }
}


/**
 * Unbinds a task from all associated vectors, but not from memory.
 * @param task - A pointer to the task to unbind.
 */
void unbindTask (Task* task) {
    for (int i=0; i<gTasks_byStatus.size(); i++) {
        if (gTasks_byStatus[i] == task) {
            gTasks_byStatus.erase(gTasks_byStatus.begin() + i);
        }
    }

    for (int i=0; i<gTasks_byPriority.size(); i++) {
        if (gTasks_byPriority[i] == task) {
            gTasks_byPriority.erase(gTasks_byPriority.begin() + i);
        }
    }

    for (int i=0; i<gTasks_byDate.size(); i++) {
        if (gTasks_byDate[i] == task) {
            gTasks_byDate.erase(gTasks_byDate.begin() + i);
        }
    }
}


/**
 * Fills in the information for, and creates, a new Task.
 * Adds the new task into all vectors in the appropriate position.
 * @see newTask(Task* task)
 */
void createTask () {
    // Create new task
    Task* task = new Task ("Not started");

    // If the task creation was aborted (the task is empty), remove it from memory and cancel
    if (task->getName().empty()) {
        delete task;
        return;
    }

    // Add the task into it's appropriate position in the vectors
    bindTask(task);
}


/**
 * Deletes the task from both memories and all associated vectors.
 * @param task - The task to delete.
 */
void deleteTask (Task* task) {
    unbindTask(task);
    delete task;
}


/**
 * Asks for an order to delete and deletes it.
 */
void deleteTask () {
    // Ask for a task number(s)
    std::string nums_str;
    std::cout << "Task number(s): "; std::getline(std::cin, nums_str);
    std::vector <int> nums = convNums(nums_str);

    // Find all tasks with the corresponding task numbers
    std::vector <Task*> deleteTasks;
    std::vector <int>   notFoundTasks;
    for (int i=0; i<nums.size(); i++) {
        Task* task = getTask(nums[i]-1);
        if (task == nullptr) {
            notFoundTasks.push_back(nums[i]);
        } else {
            deleteTasks.push_back(task);
        }
    }

    // Inform the user about which tasks are about to be removed, if any.
    if (deleteTasks.size() == 0) {
        std::cout << "No task(s) with the given numbers were found." << std::endl;
        return;
    } 
    
    std::cout << "\nAre you sure you want to delete the following task(s)?" << std::endl;
    for (int i=0; i<deleteTasks.size(); i++) {
        std::cout << "\t" << deleteTasks[i]->getName() << std::endl;
    }

    // Inform the user if any of the task numbers did not have corresponding tasks
    if (notFoundTasks.size() > 0) {
        std::cout << "(The following task number(s) were ignored as there were no corresponding task(s): ";
        for (int i=0; i<notFoundTasks.size(); i++)
            std::cout << notFoundTasks[i] << ((i==notFoundTasks.size()-1) ? "" : ", ");
        std::cout << ")" << std::endl;
    }

    // Ask user to verify their decision
    std::cout << std::endl;
    char userVertify; userVertify = createMenu (
        Unordered_map {
            {'Y', "Yes"},
            {'N', "No"}
        },
        1
    );

    switch (userVertify) {
        case 'Y':
            for (int i=0; i<deleteTasks.size(); i++)
                deleteTask(deleteTasks[i]);
            break;

        case 'N':
            std::cout << "Aborted.\n";
            break;
    }
}


/**
 * Asks for a status to edit the status of.
 * @see Task::lesStatus()
 */
void editTaskStatus () {
    // Ask for a task number
    Task* task = getTask( lesInt("Task number", 1, (*gTasks_ptr).size()) - 1 );  

    // Edit the task
    task->lesStatus();

    // Re-sort it
    unbindTask(task);
    bindTask(task);
}


/**
 * Changes the sorting order to a given attribute.
 * @param sortBy - The attribute to sort by, as a char.
 */
void editSortingOrder ( char sortBy ) {
    // Find the global vector that corresponds to 'sortBy'
    std::vector <Task*>* targetVector = 
        sortBy == 'D' ? &gTasks_byDate :
        sortBy == 'P' ? &gTasks_byPriority : 
        sortBy == 'S' ? &gTasks_byStatus :
        nullptr;

    // Invert sorting order if it's already sorted by that vector, otherwise set it to that vector.
    if (gTasks_ptr == targetVector) invertedSorting = invertedSorting ? false : true;
    else gTasks_ptr = targetVector;
}


/**
 * Asks for an attribute to sort by and changes the sorting order to that.
 * @see editSortingOrder(char sortBy)
 */
void editSortingOrder () {
    std::cout << "\nSort by" << std::endl;
    char userSortBy; userSortBy = createMenu (
        Unordered_map {
            {'D', "Date"},
            {'P', "Priority"},
            {'S', "Status"}
        },
        1
    );

    editSortingOrder(userSortBy);
}


/**
 * Sets the width of the text-window to a given amount of chars.
 * @param width - The new width of the text-window.
 */
void setWidth(int width) {
    screen_width        = width;
    id_width            = 3 + (screen_width-41)/24;
    task_width          = 5 + (screen_width-41)/4;
    date_width          = 5 + (screen_width-41)/24;
    description_width   = 12 + (screen_width-41)/2;
    status_width        = 7 + (screen_width-41)/8;
    priority_width      = 9 + (screen_width-41)/24;
}


/**
 * Asks for a new width and sets the width to that.
 */
void changeWidth() {
    setWidth(lesInt("Width of screen", 41, 200));
}


/**
 * Saves the current state to file.
 */
void saveToFile() {
    std::ofstream file (filename);
    if (file.is_open()) {
        file << screen_width << std::endl;
        for (int i=0; i<gTasks_byStatus.size(); i++) {
            gTasks_byStatus[i]->skrivData_raw(file);
        }
    }
}


/**
 * Loads from file.
 */
void loadFromFile() {
    int lineNr = 0;
    std::string line;
    std::ifstream file (filename);
    if (file.is_open()) {
        while ( getline (file,line) ) {
            lineNr++;

            // Load UI width (if applicable)
            if (lineNr == 1) {
                if (is_number(line)) {
                    screen_width        = std::stoi(line);
                    id_width            = 3 + (screen_width-41)/24;
                    task_width          = 5 + (screen_width-41)/4;
                    date_width          = 5 + (screen_width-41)/24;
                    description_width   = 12 + (screen_width-41)/2;
                    status_width        = 7 + (screen_width-41)/8;
                    priority_width      = 9 + (screen_width-41)/24;
                    continue;
                }
            }

            // Load vars and create task
            std::vector <std::string> vars = explode(line, '\t');
            Task* task = new Task;
            task->setName(vars[0]);
            task->setDescription(vars[1]);
            task->setDueDay(stoi(vars[2]));
            task->setDueMonth(stoi(vars[3]));
            task->setPriority(stoi(vars[4]));
            task->setStatus(vars[5]);
            task->setList(vars[6]);

            // Add task to local memory
            bindTask(task);
        }
        file.close();
    }
}


/**
 * The main function.
 */
int main()
{
    loadFromFile();

    char    userCommand = '\0';
    bool    done    = false;

    while (!done) {
        ClearScreen();
        skrivTasks();

        userCommand = createMenu (getMenuOptions(), 1);

        switch (userCommand) {
            case 'Q':
                done = true;
                break;

            case 'A':
                createTask();
                break;

            case 'E':
                editTaskStatus();
                break;

            case 'S':
                editSortingOrder();
                break;

            case 'D':
                deleteTask();
                break;

            case 'W':
                changeWidth();
                break;
        }
    }

    saveToFile();
    return 0;
}